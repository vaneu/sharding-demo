package com.vaneu.test;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.vaneu.sharding.Application;
import com.vaneu.sharding.entity.Dict;
import com.vaneu.sharding.service.IDictService;

/**
 * @ClassName: TestCase
 * @Description: TODO()
 * @author vaneu
 * @date 2020-12-8 16:34:22
 */
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableTransactionManagement // 如果mybatis中service实现类中加入事务注解，需要此处添加该注解
@EnableAutoConfiguration
@ActiveProfiles("dev")
public class DictTestCase {
	
	private @Autowired IDictService dictService;
	
	// 查询ds1分表
	@Test
	public void list() {
		try {
			dictService.list().forEach(System.out::println);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void add() {
		try {
			for(int i = 1; i<=10; i++) {
				dictService.save(new Dict()
						.setId(Long.valueOf(i))
						.setType("gender")
						.setTypeName("性别")
						.setCode("G_" + i)
						.setName("男女")
						.setUpdateUser("admin")
						.setUpdateTime(new Date()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
