package com.vaneu.sharding.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vaneu.sharding.entity.Dict;
import com.vaneu.sharding.mapper.DictMapper;
import com.vaneu.sharding.service.IDictService;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 * @author vaneu
 * @since 2020-12-08
 */
@DS("sharding")
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements IDictService {

}
