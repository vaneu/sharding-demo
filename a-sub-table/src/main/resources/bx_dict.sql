/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 80020
Source Host           : 127.0.0.1:3306
Source Database       : db1

Target Server Type    : MYSQL
Target Server Version : 80020
File Encoding         : 65001

Date: 2020-12-10 14:24:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bx_dict_1
-- ----------------------------
DROP TABLE IF EXISTS `bx_dict_1`;
CREATE TABLE `bx_dict_1` (
  `id` bigint NOT NULL COMMENT '主键',
  `type` varchar(20) DEFAULT NULL COMMENT '类型',
  `type_name` varchar(20) DEFAULT NULL COMMENT '类型名称',
  `code` varchar(255) DEFAULT NULL COMMENT '代码',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `update_user` varchar(100) DEFAULT NULL COMMENT '更新人',
  `create_user` varchar(100) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int DEFAULT '1' COMMENT '状态：0.禁用，1.启用',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Table structure for bx_dict_2
-- ----------------------------
DROP TABLE IF EXISTS `bx_dict_2`;
CREATE TABLE `bx_dict_2` (
  `id` bigint NOT NULL COMMENT '主键',
  `type` varchar(20) DEFAULT NULL COMMENT '类型',
  `type_name` varchar(20) DEFAULT NULL COMMENT '类型名称',
  `code` varchar(255) DEFAULT NULL COMMENT '代码',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `update_user` varchar(100) DEFAULT NULL COMMENT '更新人',
  `create_user` varchar(100) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int DEFAULT '1' COMMENT '状态：0.禁用，1.启用',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';
