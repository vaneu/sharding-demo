package com.vaneu.sharding.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author vaneu
 * @since 2020-12-08
 */
@Data
@Accessors(chain = true)
@TableName("bx_dict")
public class Dict implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.INPUT)
	private Long id;

	/**
	 * 类型
	 */
	@TableField("type")
	private String type;

	/**
	 * 类型名称
	 */
	@TableField("type_name")
	private String typeName;

	/**
	 * 代码
	 */
	@TableField("code")
	private String code;

	/**
	 * 名称
	 */
	@TableField("name")
	private String name;

	/**
	 * 更新人
	 */
	@TableField("update_user")
	private String updateUser;

	/**
	 * 创建人
	 */
	@TableField("create_user")
	private String createUser;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 状态：0.禁用，1.启用
	 */
	@TableField("status")
	private Integer status;

	/**
	 * 版本
	 */
	@TableField("version")
	private Long version;
}
