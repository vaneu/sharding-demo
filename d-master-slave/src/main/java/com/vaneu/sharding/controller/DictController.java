package com.vaneu.sharding.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vaneu.sharding.entity.Dict;
import com.vaneu.sharding.service.IDictService;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author vaneu
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/dict")
public class DictController {

	@Autowired
	IDictService dictService;

	@GetMapping("/add/{id}")
	public Boolean add(@PathVariable Long id) {
		dictService.save(new Dict()
			.setId(id)
			.setType("gender")
			.setTypeName("性别")
			.setCode("G_" + id)
			.setName("男女")
			.setUpdateUser("admin")
			.setUpdateTime(new Date())
		);
		return true;
	}
	
	@GetMapping("/get")
	public List<Dict> get() {
		return dictService.list();
	}
}
