package com.vaneu.sharding.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vaneu.sharding.entity.Dict;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author vaneu
 * @since 2020-12-08
 */
public interface DictMapper extends BaseMapper<Dict> {

}
