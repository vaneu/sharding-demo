package com.vaneu.sharding.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vaneu.sharding.entity.Dict;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author vaneu
 * @since 2020-12-08
 */
public interface IDictService extends IService<Dict> {

}
