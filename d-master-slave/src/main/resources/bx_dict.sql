DROP TABLE IF EXISTS `bx_dict`;
CREATE TABLE `bx_dict` (
  `id` bigint NOT NULL COMMENT '主键',
  `type` varchar(20) DEFAULT NULL COMMENT '类型',
  `type_name` varchar(20) DEFAULT NULL COMMENT '类型名称',
  `code` varchar(255) DEFAULT NULL COMMENT '代码',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `update_user` varchar(100) DEFAULT NULL COMMENT '更新人',
  `create_user` varchar(100) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int DEFAULT '1' COMMENT '状态：0.禁用，1.启用',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';
