package com.vaneu.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.vaneu.sharding.Application;
import com.vaneu.sharding.entity.Gift;
import com.vaneu.sharding.service.IGiftService;

/**
 * @ClassName: TestCase
 * @Description: TODO()
 * @author vaneu
 * @date 2020-12-8 16:34:22
 */
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableTransactionManagement // 如果mybatis中service实现类中加入事务注解，需要此处添加该注解
@EnableAutoConfiguration
@ActiveProfiles("dev")
public class GiftTestCase {
	
	private @Autowired IGiftService giftService;
	
	// 插入DS1和DS2
	@Test
	public void add() {
		for(int i = 1; i<=20; i++) {
			giftService.save(new Gift()
				.setId(Long.valueOf(i))
				.setName("红玫瑰")
				.setPrice(i)
				.setUnit("朵")
				.setImage("http://benx.oss-cn-shenzhen.aliyuncs.com/default/gift_2.png?Expires=3134022800&OSSAccessKeyId=LTAIZk9Vxvkf2g0M&Signature=rN2cm8TmGiSvcWXHAB3dGoGA%2F7I%3D")
			);
		}
	}
	
	// 查询DS1和DS2
	@Test
	public void get() {
		Gift gift = giftService.getById(5L);
		System.out.println(gift);
	}
	
	// 查询DS1和DS2
	@Test
	public void list() {
		giftService.list().forEach(System.out::println);
	}
}
