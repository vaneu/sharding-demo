package com.vaneu.sharding.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vaneu.sharding.entity.Gift;

/**
 * <p>
 * 礼物类型表 服务类
 * </p>
 *
 * @author vaneu
 * @since 2020-12-08
 */
public interface IGiftService extends IService<Gift> {

}
