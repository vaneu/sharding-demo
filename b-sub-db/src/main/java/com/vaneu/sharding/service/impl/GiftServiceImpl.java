package com.vaneu.sharding.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vaneu.sharding.entity.Gift;
import com.vaneu.sharding.mapper.GiftMapper;
import com.vaneu.sharding.service.IGiftService;

/**
 * <p>
 * 礼物类型表 服务实现类
 * </p>
 *
 * @author vaneu
 * @since 2020-12-08
 */
@DS("sharding")
@Service
public class GiftServiceImpl extends ServiceImpl<GiftMapper, Gift> implements IGiftService {

	
}
