package com.vaneu.sharding.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 礼物类型表
 * </p>
 *
 * @author vaneu
 * @since 2020-12-08
 */
@Data
@Accessors(chain = true)
@TableName("bx_gift")
public class Gift implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.INPUT)
	private Long id;

	/**
	 * 礼物名称
	 */
	@TableField("name")
	private String name;

	/**
	 * 单价，单位:分
	 */
	@TableField("price")
	private Integer price;

	/**
	 * 单位
	 */
	@TableField("unit")
	private String unit;

	/**
	 * 图片URL
	 */
	@TableField("image")
	private String image;

	/**
	 * 状态:0.禁用,1.启用
	 */
	@TableField("status")
	private Integer status;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 更新时间
	 */
	@TableField("update_time")
	private Date updateTime;

	/**
	 * 版本
	 */
	@TableField("version")
	private Long version;

}
