package com.vaneu.sharding.mapper;

import com.vaneu.sharding.entity.Gift;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 礼物类型表 Mapper 接口
 * </p>
 *
 * @author vaneu
 * @since 2020-12-08
 */
public interface GiftMapper extends BaseMapper<Gift> {

}
