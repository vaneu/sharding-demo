/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 80020
Source Host           : 127.0.0.1:3306
Source Database       : db1

Target Server Type    : MYSQL
Target Server Version : 80020
File Encoding         : 65001

Date: 2020-12-09 09:23:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bx_gift
-- ----------------------------
DROP TABLE IF EXISTS `bx_gift`;
CREATE TABLE `bx_gift` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(50) NOT NULL COMMENT '礼物名称',
  `price` int NOT NULL DEFAULT '0' COMMENT '单价，单位:分',
  `unit` varchar(50) NOT NULL COMMENT '单位',
  `image` varchar(1024) NOT NULL COMMENT '图片URL',
  `status` tinyint NOT NULL DEFAULT '1' COMMENT '状态:0.禁用,1.启用',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `version` bigint NOT NULL DEFAULT '0' COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='礼物类型表';