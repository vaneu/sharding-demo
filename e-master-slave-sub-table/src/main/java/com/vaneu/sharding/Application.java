package com.vaneu.sharding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName: Application
 * @Description: TODO()
 * @author vaneu
 * @date 2020-12-10 16:19:22
 */
@SpringBootApplication(exclude = { })
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}