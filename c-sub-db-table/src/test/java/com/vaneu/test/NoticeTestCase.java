package com.vaneu.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.vaneu.sharding.Application;
import com.vaneu.sharding.entity.Notice;
import com.vaneu.sharding.service.INoticeService;

/**
 * @ClassName: TestCase
 * @Description: TODO()
 * @author vaneu
 * @date 2020-12-9 13:24:18
 */
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableTransactionManagement // 如果mybatis中service实现类中加入事务注解，需要此处添加该注解
@EnableAutoConfiguration
@ActiveProfiles("dev")
public class NoticeTestCase {
	
	private @Autowired INoticeService noticeService;
	
	// 插入DS1和DS2
	@Test
	public void add() {
		try {
			for(int i = 1; i<=20; i++) {
				noticeService.save(new Notice()
						.setId(Long.valueOf(i))
						.setMobile("1870000000")
						.setNickname("测试_" + i)
				);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// 查询DS1和DS2
	@Test
	public void list() {
		noticeService.list(new QueryWrapper<Notice>().orderByAsc("id")).forEach(System.out::println);
	}
	
	@Test
	public void mod() {
		try {
			noticeService.updateById(new Notice()
					.setId(3L)
					.setMobile("11111111111")
					.setNickname("333吃")
			);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
