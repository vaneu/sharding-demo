package com.vaneu.sharding.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 封停公告
 * </p>
 *
 * @author vaneu
 * @since 2020-12-12
 */
@Data
@Accessors(chain = true)
@TableName("bx_notice")
public class Notice implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 手机号码
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 昵称
     */
    @TableField("nickname")
    private String nickname;

    /**
     * 封停时间
     */
    @TableField("thaw_time")
    private Date thawTime;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


}
