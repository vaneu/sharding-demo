package com.vaneu.sharding.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 封停公告 前端控制器
 * </p>
 *
 * @author vaneu
 * @since 2020-12-12
 */
@Controller
@RequestMapping("/notice")
public class NoticeController {

}

