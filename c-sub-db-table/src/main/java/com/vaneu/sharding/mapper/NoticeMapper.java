package com.vaneu.sharding.mapper;

import com.vaneu.sharding.entity.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 封停公告 Mapper 接口
 * </p>
 *
 * @author vaneu
 * @since 2020-12-12
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}
