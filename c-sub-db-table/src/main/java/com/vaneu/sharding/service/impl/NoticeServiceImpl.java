package com.vaneu.sharding.service.impl;

import com.vaneu.sharding.entity.Notice;
import com.vaneu.sharding.mapper.NoticeMapper;
import com.vaneu.sharding.service.INoticeService;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 封停公告 服务实现类
 * </p>
 *
 * @author vaneu
 * @since 2020-12-12
 */
@DS("sharding")
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements INoticeService {

}
