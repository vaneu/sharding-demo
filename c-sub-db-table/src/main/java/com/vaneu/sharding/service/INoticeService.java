package com.vaneu.sharding.service;

import com.vaneu.sharding.entity.Notice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 封停公告 服务类
 * </p>
 *
 * @author vaneu
 * @since 2020-12-12
 */
public interface INoticeService extends IService<Notice> {

}
