/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 80020
Source Host           : 127.0.0.1:3306
Source Database       : db1

Target Server Type    : MYSQL
Target Server Version : 80020
File Encoding         : 65001

Date: 2020-12-12 00:24:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bx_notice_1
-- ----------------------------
DROP TABLE IF EXISTS `bx_notice_1`;
CREATE TABLE `bx_notice_1` (
  `id` bigint NOT NULL,
  `mobile` varchar(11) NOT NULL COMMENT '手机号码',
  `nickname` varchar(200) NOT NULL COMMENT '昵称',
  `thaw_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '封停时间',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='封停公告';

-- ----------------------------
-- Table structure for bx_notice_2
-- ----------------------------
DROP TABLE IF EXISTS `bx_notice_2`;
CREATE TABLE `bx_notice_2` (
  `id` bigint NOT NULL,
  `mobile` varchar(11) NOT NULL COMMENT '手机号码',
  `nickname` varchar(200) NOT NULL COMMENT '昵称',
  `thaw_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '封停时间',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='封停公告';

-- ----------------------------
-- Table structure for bx_notice_3
-- ----------------------------
DROP TABLE IF EXISTS `bx_notice_3`;
CREATE TABLE `bx_notice_3` (
  `id` bigint NOT NULL,
  `mobile` varchar(11) NOT NULL COMMENT '手机号码',
  `nickname` varchar(200) NOT NULL COMMENT '昵称',
  `thaw_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '封停时间',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='封停公告';
